package com.mol.routing_gui.ui;

import com.mol.routing_gui.ui.view.ambiente_list.AmbienteListView;
import com.mol.routing_gui.ui.view.matrice.MatriceView;
import com.mol.routing_gui.ui.view.partner_list.PartnerListView;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.*;

//Layout principale
@CssImport("./styles/shared-styles.css")
@PWA(
        name = "RoutingGUI",
        shortName = "R-GUI"
)
public class MainLayout extends AppLayout implements AppShellConfigurator, VaadinServiceInitListener {
    public MainLayout() {
        createHeader();
        createDrawer();
    }

    //Disabilita tema di default per usare indicatore custom
    @Override
    public void serviceInit(ServiceInitEvent serviceInitEvent) {
        serviceInitEvent.getSource().addUIInitListener(uiInitEvent -> {
            uiInitEvent.getUI().getLoadingIndicatorConfiguration()
                    .setApplyDefaultTheme(false);
        });
    }

    private void createHeader() {
        H1 logo = new H1("Routing GUI");
        logo.addClassName("logo");

        Anchor logout = new Anchor("logout", "Log out");

        HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), logo, logout);

        header.setDefaultVerticalComponentAlignment(
                FlexComponent.Alignment.CENTER);
        header.setWidth("100%");
        header.addClassName("header");


        addToNavbar(header);

    }

    //Crea drawer di navigazione e imposta le routes
    private void createDrawer() {
        RouterLink partnerLink = new RouterLink("Partner", PartnerListView.class);
        RouterLink ambientiLink = new RouterLink("Ambienti", AmbienteListView.class);
        RouterLink matriceLink = new RouterLink("Routing", MatriceView.class);
        matriceLink.setHighlightCondition(HighlightConditions.sameLocation());

        addToDrawer(new VerticalLayout(partnerLink, ambientiLink, matriceLink ));
    }
}