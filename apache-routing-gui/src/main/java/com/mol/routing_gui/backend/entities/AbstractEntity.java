package com.mol.routing_gui.backend.entities;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;

/*Entità astratta*/
@MappedSuperclass
public abstract class AbstractEntity {
    //ATTRIBUTI
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    public Long getId(){
        return id;
    }

    public boolean isPersisted(){
        return id!=null;
    }

    @Override
    public int hashCode(){
        return isPersisted()? id.hashCode() : super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractEntity other = (AbstractEntity) obj;
        if (getId() == null || other.getId() == null) {
            return false;
        }
        return getId().equals(other.getId());
    }
}
