package com.mol.routing_gui.ui.view.partner_list;

import com.mol.routing_gui.backend.entities.Partner;
import com.mol.routing_gui.backend.service.PartnerService;
import com.mol.routing_gui.backend.utilities.ShellCommands;
import com.mol.routing_gui.ui.MainLayout;
import com.vaadin.annotations.Push;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.InputStreamFactory;
import com.vaadin.flow.server.StreamResource;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Component
@Scope("prototype")
@Route(value = "Partner", layout = MainLayout.class)
@PageTitle("Partner | R-GUI")
@CssImport("./styles/shared-styles.css")
@Push
public class PartnerListView extends VerticalLayout {

        PartnerService partnerService;

        public Grid<Partner> grid = new Grid<>(Partner.class);
        TextField filterNome = new TextField();
        public PartnerForm form;
        Button addPartner = new Button("Aggiungi...");

        //Memorizza il partner selezionato ( per verificare differenze durante la modifica )
        Partner partnerCache = new Partner();

        //Determina se si sta modificando un partner o se ne sta creando uno nuovo
        boolean editing = false;

        //Messaggio d'errore
        final static String DUPLICATE_SQL = "ERRORE: Esiste già un partner con lo stesso nome/IP!";

        public PartnerListView(PartnerService partnerService) {

            this.partnerService = partnerService;
            addClassName("ambiente_list-view");
            setSizeFull();
            configureGrid();
            configureFilterNome();

            HorizontalLayout filters = new HorizontalLayout();
            filters.add(filterNome);
            filters.add(addPartner);
            Div filt = new Div(filters);

            addPartner.addClickListener(click -> addPartner());

            form = new PartnerForm();
            form.setVisible(false);
            form.addListener(PartnerForm.SaveEvent.class, this::savePartner);
            form.addListener(PartnerForm.DeleteEvent.class, this::deletePartner);
            form.addListener(PartnerForm.CloseEvent.class, e->closeEditor());

            form.setHeightFull();

            Div content = new Div(grid, form);

            content.addClassName("content");
            content.setSizeFull();

            add(filt, content);
            updateList();
        }

    //Apri form per aggiungere un nuovo partner
    void addPartner() {
        grid.asSingleSelect().clear();
        editPartner(new Partner());
    }

    //Salva partner modificato
    private void savePartner(PartnerForm.SaveEvent event) {
            //Nuovo partner
            if(!editing)
                partnerService.save(event.getPartner());
            //Partner modificato
            else
                updatePartner(event.getPartner());
            //Resetta status
            editing = false;
        updateList();
        closeEditor();
    }

    //Cancella partner
    private void deletePartner(PartnerForm.DeleteEvent event) {
        partnerService.delete(event.getPartner());
        try {
            //Cancella routes
            ShellCommands.deleteRoute(event.getPartner().getIp());
        }catch (IOException | InterruptedException ignored){}
        updateList();
        closeEditor();
    }

    //Modifica un partner
    private void updatePartner(Partner p) {
        try {
            //Se l'ip è stato modificato cancella la vecchia regola
            if(!partnerCache.getIp().equals(p.getIp())){
                ShellCommands.deleteRoute(partnerCache.getIp());
            }
            partnerService.update(p);
            //Modifica regola associata all'ip
            ShellCommands.refreshRoute(p.getIp(), partnerCache.getAmbiente()!=null?partnerCache.getAmbiente().getRiferimento():null);
        } catch (DataIntegrityViolationException e) {
            //Mostra notifica di errore
            showError(DUPLICATE_SQL, NotificationVariant.LUMO_ERROR);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void configureFilterNome() {
            filterNome.setPlaceholder("Filtra per IP...");
            filterNome.setClearButtonVisible(true);
            filterNome.setValueChangeMode(ValueChangeMode.LAZY);
            filterNome.addValueChangeListener(e -> updateList());
    }

    //Visualizza partner nel form
    public void editPartner(Partner partner) {
            if(partner==null){
                closeEditor();
            }
            else {
                form.setPartner(partner);
                partnerCache = partner.clone();
                form.setVisible(true);
                addClassName("editing");
            }
    }

    private void closeEditor() {
        form.setPartner(null);
        form.setVisible(false);
        removeClassName("editing");
        editing = false;
    }

    private void configureGrid() {
            grid.addClassName("partner-grid");
            grid.setSizeFull();
            grid.removeAllColumns();
            grid.addColumn("compagnia");
            grid.addComponentColumn(partner -> partner.getLogo()==null? new Label("No Logo") : convertToImage(partner.getLogo())).setHeader("Descrizione");
            grid.addColumn("ip").setHeader("IP");
            grid.asSingleSelect().addValueChangeListener(event -> { editing = true; editPartner(event.getValue());});
            grid.getColumns().forEach(col -> col.setAutoWidth(true));
    }

    //Conversione del logo per l'upload nel db
    private Image convertToImage(byte[] imageData) {
        imageData = imageData == null? new byte[0]: imageData;
        byte[] finalImageData = imageData;
        StreamResource streamResource = new StreamResource("isr", (InputStreamFactory) ()
                -> new ByteArrayInputStream(finalImageData));
        Image im = new Image(streamResource, "empty");
        im.setHeight("30px");
        im.setWidth("30px");
        return im;
    }

    //Mostra notifica di errore
    private void showError(String caption, NotificationVariant nv){
        if(getUI().isPresent()) {
            Notification n = new Notification(caption, 1500, Notification.Position.MIDDLE);
            n.addThemeVariants(nv);
            getUI().get().access(() -> {
                add(n);
                n.open();
            });
        }
    }

    //Refresh della grid
    private void updateList() {
        grid.setItems(partnerService.findAll(filterNome.getValue()));
    }
}
