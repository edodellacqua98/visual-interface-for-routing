package com.mol.routing_gui.backend.service;

import com.mol.routing_gui.backend.entities.Ambiente;
import com.mol.routing_gui.backend.entities.Partner;
import com.mol.routing_gui.backend.repository.PartnerRepository;
import com.mol.routing_gui.backend.utilities.ShellCommands;
import org.apache.commons.io.FileUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PartnerService {
    private static final Logger LOGGER = Logger.getLogger(PartnerService.class.getName());
    private final PartnerRepository partnerRepository;
    private final AmbienteService ambienteService;

    public PartnerService(PartnerRepository partnerRepository, AmbienteService ambienteService) {
        this.partnerRepository = partnerRepository;
        this.ambienteService = ambienteService;
    }

    //Immagini per logo dati default
    public static String[] sampleFiles = {
            "apache-routing-gui/src/main/resources/sample_logos/MYSQL.png",
            "apache-routing-gui/src/main/resources/sample_logos/DOCKER.png",
            "apache-routing-gui/src/main/resources/sample_logos/SPRING.png",
            "apache-routing-gui/src/main/resources/sample_logos/APACHE.png",
            "apache-routing-gui/src/main/resources/sample_logos/SPRINGBOOT.png"
    };

    public List<Partner> findAll() {
        return partnerRepository.findAll();
    }

    public List<Partner> findAll(String filterText) {
        if(filterText==null || filterText.isEmpty())
            return partnerRepository.findAll();
        else
            return partnerRepository.search(filterText);
    }

    public void delete(Partner partner) {
        partnerRepository.delete(partner);
    }

    //Modifica di un partner
    public void update(Partner partner) throws DataIntegrityViolationException {
        if( partner == null) {
            LOGGER.log(Level.SEVERE,
                    "ERRORE: Partner nullo");
        }
        else
            partnerRepository.update(partner.getCompagnia(), partner.getIp(), partner.getId(), partner.getLogo());
    }

    //Salvataggio di un partner nuovo
    public void save(Partner partner) {
        if (partner == null) {
            LOGGER.log(Level.SEVERE,
                    "ERRORE: Partner nullo");
            return;
        }
    if(partnerRepository.findOne(partner.getIp())==null)
            partnerRepository.save(partner);
    }

    //TEST DATA
    @PostConstruct
    public void getData() throws IOException {

              AtomicInteger x = new AtomicInteger(1);
        
              List<Ambiente> ambienti = Stream.of("Ambiente1", "Ambiente2", "Ambiente3", "Ambiente4", "Ambiente5")
                        .map(name -> {
                            Ambiente ambiente = new Ambiente();
                            ambiente.setNome(name);
                            ambiente.setDescrizione("desc_placeholder");
                            ambiente.setRiferimento("test"+ x.getAndIncrement());
                            return ambiente;
                        }).collect(Collectors.toList());

              for(Ambiente a: ambienti){
                  ambienteService.save(a);
              }

              List<String> ips = Stream.of("172.18.238.10", "172.18.238.20", "172.18.238.30", "172.18.238.40", "172.18.238.50", "172.18.238.60").collect(Collectors.toList());

              AtomicInteger n = new AtomicInteger();

              List<Partner> partners = Stream.of("partner1", "partner2", "partner3", "partner4", "partner5")
                      .map(name ->{
                          Partner partner = new Partner();
                          partner.setIp(ips.get(n.get()));
                          partner.setCompagnia(name);

                          try {
                              partner.setLogo(FileUtils.readFileToByteArray(new File(sampleFiles[n.get()])));
                          } catch (IOException e) {
                              partner.setLogo(null);
                          }
                          n.getAndIncrement();
                          return partner;
                      }).collect(Collectors.toList());

              for(Partner p: partners){
                  save(p);
              }

        ShellCommands.getRoutes(this, ambienteService);
    }
}
