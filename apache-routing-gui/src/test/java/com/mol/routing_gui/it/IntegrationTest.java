package com.mol.routing_gui.it;

import com.vaadin.flow.component.button.testbench.ButtonElement;
import com.vaadin.flow.component.checkbox.testbench.CheckboxElement;
import com.vaadin.flow.component.grid.testbench.GridElement;
import com.vaadin.flow.component.login.testbench.LoginFormElement;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class IntegrationTest extends AbstractTest {
    public IntegrationTest(){
        super("");
    }

    //Testa il login
    @Test
    public void testLogin(){
        LoginFormElement form = $(LoginFormElement.class).first();
        //Inserisci credenziali
        form.getUsernameField().setValue("user");
        form.getPasswordField().setValue("password");
        form.getSubmitButton().click();
        //Prova a loggare
        Assert.assertFalse($(LoginFormElement.class).exists());
    }

    //Testa il salvataggio delle routes
    @Test
    public void testRoute() {
        LoginFormElement form = $(LoginFormElement.class).first();
        //Inserisci credenziali e logga
        form.getUsernameField().setValue("user");
        form.getPasswordField().setValue("password");
        form.getSubmitButton().click();

        GridElement grid = $(GridElement.class).first();
        List<ButtonElement> allButtons = $(ButtonElement.class).all();
        ButtonElement salva = new ButtonElement();

        //Trova il bottone salva
        for(ButtonElement b: allButtons) {
            if (b.getText().equals("Salva"))
                salva = b;
        }

        int rowNumber = grid.getRowCount();

        List<CheckboxElement> chbElements = $(CheckboxElement.class).all();

        //Seleziona le checkbox sulla diagionale
        for(int i=0; i<rowNumber-1; i++){
            chbElements.get(i*(rowNumber-1)).setChecked(true);
        }

        //Clicca il bottone salva
        salva.click();

        chbElements = $(CheckboxElement.class).all();

        //Controlla i risultati dopo il refresh
        for(int i=0; i<grid.getRowCount()-1; i++){
            Assert.assertTrue(chbElements.get(i*(rowNumber-1)).isChecked());
        }
    }

    @After
    public void quit(){
        getDriver().quit();
    }

}
