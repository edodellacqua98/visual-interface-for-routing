package com.mol.routing_gui.view.list;

import com.mol.routing_gui.backend.entities.Partner;
import com.mol.routing_gui.ui.view.partner_list.PartnerForm;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicReference;

//Testa il corretto funzionamento del form di modifica dei partner
public class PartnerFormTest {
    private Partner partner;

    @Before
    public void setupData(){
        partner = new Partner();
        partner.setCompagnia("Compagnia");
        partner.setIp("192.168.1.1");
        partner.setLogo(null);
    }

    @Test
    public void formFieldsPopulated() {
        PartnerForm form = new PartnerForm();
        form.setPartner(partner);
        Assert.assertEquals("Compagnia", form.compagnia.getValue());
        Assert.assertEquals("ip", form.ip.getValue());
        Assert.assertNull(form.logo);
    }

    @Test
    public void saveEventHasCorrectValues(){
        PartnerForm form = new PartnerForm();
        Partner partner2 = new Partner();
        form.setPartner(partner2);

        form.compagnia.setValue("Compagnia");
        form.ip.setValue("192.168.1.1");
        form.logo = null;

        AtomicReference<Partner> savedPartnerRef = new AtomicReference<>(null);
        form.addListener(PartnerForm.SaveEvent.class, e -> {
            savedPartnerRef.set(e.getPartner());
        });
        form.salva.click();
        Partner savedPartner = savedPartnerRef.get();

        Assert.assertEquals("Compagnia", savedPartner.getCompagnia());
        Assert.assertEquals("192.168.1.1", savedPartner.getIp());
        Assert.assertNull(savedPartner.getLogo());
    }
}
