package com.mol.routing_gui.backend.repository;

import com.mol.routing_gui.backend.entities.Ambiente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AmbienteRepository extends JpaRepository<Ambiente, Long> {
    //Ricerca per filtro
    @Query("select c from Ambiente c " +
            "where lower(c.riferimento) like lower(concat('%', :searchTerm, '%'))")
    List<Ambiente> search(@Param("searchTerm") String searchTerm); //
}
