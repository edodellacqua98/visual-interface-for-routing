package com.mol.routing_gui.ui.view.partner_list;

import com.mol.routing_gui.backend.entities.Partner;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import org.apache.commons.compress.utils.IOUtils;
import java.io.IOException;
import java.util.regex.Pattern;

public class PartnerForm extends FormLayout {

    //ATTRIBUTI
    public byte[] logo;
    public TextField compagnia = new TextField("compagnia");
    public TextField ip = new TextField("IP");

    public Button salva = new Button("Salva");
    public Button elimina = new Button("Elimina");
    public Button cancella = new Button("Cancella");

    Binder<Partner> binder = new BeanValidationBinder<>(Partner.class);

    public PartnerForm(){
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);

        //Upload di un'immagine per il logo
        upload.addSucceededListener(event -> {
            try {
                logo = IOUtils.toByteArray(buffer.getInputStream());
                Partner p = binder.getBean();
                p.setLogo(logo);
                binder.setBean(p);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        addClassName("partner-form");

        binder.bindInstanceFields(this);

        //Aggiunge un validator per il binder ( per controllare il valore dell'ip )
        binder.forField(ip)
                .withValidator(IpAddressValidator::isValid, "IP non valido").bind(Partner::getIp, Partner::setIp);

        add(new VerticalLayout(new HorizontalLayout(compagnia, ip, upload),
            createButtonsLayout()));

        salva.addClickListener(event -> validateAndSave());
    }

    private HorizontalLayout createButtonsLayout(){
        salva.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        elimina.addThemeVariants(ButtonVariant.LUMO_ERROR);
        cancella.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        salva.addClickShortcut(Key.ENTER);
        cancella.addClickShortcut(Key.ESCAPE);

        salva.addClickListener(event -> validateAndSave());
        elimina.addClickListener(event -> fireEvent(new DeleteEvent(this, binder.getBean())));
        cancella.addClickListener(event -> fireEvent(new CloseEvent(this)));

        binder.addStatusChangeListener(e -> salva.setEnabled(binder.isValid()));
        return new HorizontalLayout(salva, elimina, cancella);
    }

    private void validateAndSave() {
        if (binder.isValid()) {
            fireEvent(new SaveEvent(this, binder.getBean()));
        }
    }

    public void setPartner(Partner partner) {
        binder.setBean(partner);
    }

    //Metodo per verificare la correttezza dell'indirizzo ip
    public static class IpAddressValidator {

        //REGEX
        private static final String zeroTo255
                = "([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])";

        private static final String IP_REGEXP
                = zeroTo255 + "\\." + zeroTo255 + "\\."
                + zeroTo255 + "\\." + zeroTo255;

        private static final Pattern IP_PATTERN  = Pattern.compile(IP_REGEXP);

        // Restituisce true se l'ip è valido
        private static boolean isValid(String address) {
            return IP_PATTERN.matcher(address).matches();
        }
    }

    //EVENTI
    public static abstract class PartnerFormEvent extends ComponentEvent<PartnerForm> {
        private final Partner partner;

        protected PartnerFormEvent(PartnerForm source, Partner partner) {
            super(source, false);
            this.partner = partner;
        }

        public Partner getPartner() {
            return partner;
        }
    }

    public static class SaveEvent extends PartnerFormEvent {
        SaveEvent(PartnerForm source, Partner partner) {
            super(source, partner);
        }
    }

    public static class DeleteEvent extends PartnerFormEvent {
        DeleteEvent(PartnerForm source, Partner partner) {
            super(source, partner);
        }

    }

    public static class CloseEvent extends PartnerFormEvent {
        CloseEvent(PartnerForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}

