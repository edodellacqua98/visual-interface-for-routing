Tirocinio Formativo Universitario presso MutuiOnline spa.
Implementa uno strumento grafico per la gestione semplificata delle routing policies definite su un web Server eseguito in un container di Docker.
Permette la gestione e il censimento dei partner, il controllo dell'accesso per gli utenti e la modifica effettiva delle regole per Apache mediante una matrice di Checkbox.
Per le sua esecuzione richiede ovviamente che tutti i container dell'architettura lato Docker siano correttamente in esecuzione.
