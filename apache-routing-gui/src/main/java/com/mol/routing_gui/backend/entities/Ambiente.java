package com.mol.routing_gui.backend.entities;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

//Ambienti
@Entity
@Table(name = "Ambiente")
public class Ambiente extends AbstractEntity implements Cloneable{

    //Costruttore vuoto per Hibernate
    public Ambiente(){}

    //ATTRIBUTI
    @NotNull
    @NotEmpty
    private String nome;

    @NotNull
    @NotEmpty
    private String descrizione;

    @OneToMany
    @JoinColumn(name = "ambiente")
    private final List<Partner> partnerList = new LinkedList<>();

    @NotNull
    @NotEmpty
    private String riferimento;

    @PreRemove
    private void preRemove() {
        for (Partner p : partnerList) {
            p.setAmbiente(null);
        }
    }

    public String getRiferimento(){return this.riferimento;}

    public void setRiferimento(String riferimento){this.riferimento=riferimento;}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

}
