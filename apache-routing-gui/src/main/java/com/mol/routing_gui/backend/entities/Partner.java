package com.mol.routing_gui.backend.entities;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

//Partner
@Entity
@Table(name = "Partner")
public class Partner extends AbstractEntity implements Cloneable{

    //ATTRIBUTI
    @Lob
    @Column(name = "logo", columnDefinition="BLOB")
    private byte[] logo;

    @NotNull
    @NotEmpty
    @Column(unique = true)
    private String ip;

    @ManyToOne(optional = true)
    @JoinColumn(name = "ambiente")
    private Ambiente ambiente;

    @NotNull
    @NotEmpty
    @Column(unique = true)
    private String compagnia;

    //Crea una copia del partner
    public Partner clone(){
        Partner p = new Partner();
        p.setLogo(getLogo());
        p.setAmbiente(getAmbiente());
        p.setIp(getIp());
        p.setCompagnia(getCompagnia());
        return p;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Ambiente getAmbiente() {
        return ambiente;
    }

    public String getNomeAmbiente() { return ambiente.getNome();}

    public void setAmbiente(Ambiente ambiente) {
        this.ambiente = ambiente;
    }

    public String getCompagnia() {
        return compagnia;
    }

    public void setCompagnia(String nome) {
        this.compagnia = nome;
    }

    @Override
    public String toString(){
        return compagnia + Arrays.toString(logo) +" " + ip;
    }
}
