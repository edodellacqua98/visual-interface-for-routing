package com.mol.routing_gui.ui.view.matrice;

import com.mol.routing_gui.backend.entities.Ambiente;
import com.mol.routing_gui.backend.entities.Partner;
import com.mol.routing_gui.backend.service.AmbienteService;
import com.mol.routing_gui.backend.service.PartnerService;
import com.mol.routing_gui.backend.utilities.ShellCommands;
import com.mol.routing_gui.ui.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Pagine con la matrice contenente le routing policies
@Component
@Scope("prototype")
@Route(value = "", layout = MainLayout.class)
public class MatriceView extends VerticalLayout {

    private final PartnerService partnerService;
    private final AmbienteService ambienteService;
    public Grid<Partner> grid = new Grid<>(Partner.class);
    public Button save = new Button("Salva");
    public Button refresh = new Button("Aggiorna");

    //Contenitore temporaneo per gli update
    private static final List<Partner> updates = new ArrayList<Partner>();

    public MatriceView(PartnerService partnerService, AmbienteService ambienteService){
        this.partnerService = partnerService;
        this.ambienteService = ambienteService;
        addClassName("matrice_list-view");
        setSizeFull();
        configureGrid();

        add(grid);

        save.addClickListener(click -> pushUpdates());
        //Ricarica la matrice di routing
        refresh.addClickListener(click -> {
            try {
                ShellCommands.getRoutes(partnerService, ambienteService);
                updateList();
            } catch (IOException ignored) {}
        });

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.add(save, refresh);

        add(buttons);
        updateList();
    }

    private void configureGrid() {
        grid.addClassName("matrice-grid");
        grid.setSizeFull();
        grid.removeAllColumns();
        grid.addColumn("compagnia").setHeader("Compagnia");
        for(Ambiente a:ambienteService.findAll())
            grid.addComponentColumn(partner -> {
                Checkbox cb = new Checkbox();
                cb.setValue(partner.getAmbiente() != null && partner.getAmbiente().getId().equals(a.getId()));
                cb.addValueChangeListener(value -> ambienteChanged(a.getRiferimento(), partner, cb));
                return cb;
            }).setHeader(a.getRiferimento());
    }

    //Salva i cambiamenti per renderli effettivi
    private void pushUpdates(){
        for(Partner p: updates){
            try {
                ShellCommands.refreshRoute(p.getIp(), p.getAmbiente()!=null? p.getAmbiente().getRiferimento() : null);
                ShellCommands.getRoutes(partnerService, ambienteService);
            }catch (IOException | InterruptedException ignored){}
        }
        updates.clear();
        updateList();
    }

    //Rileva un cambiamento nei valori delle checkbox (viene spuntata una casella)
    private void ambienteChanged(String nome, Partner partner, Checkbox cb) {

        Ambiente nuovo = ambienteService.findAll(nome).get(0);

        //Checkbox deselezionata
        if(nuovo==null || nuovo.getNome().equals(partner.getAmbiente()!=null? partner.getAmbiente().getNome(): null)) {
            partnerService.delete(partner);
            partner.setAmbiente(null);
            partnerService.save(partner);
        }
        else
            partner.setAmbiente(nuovo);

        updates.remove(partner);
        updates.add(partner);
        grid.getDataProvider().refreshItem(partner);
    }

    private void updateList() {
        grid.setItems(partnerService.findAll());
    }

}