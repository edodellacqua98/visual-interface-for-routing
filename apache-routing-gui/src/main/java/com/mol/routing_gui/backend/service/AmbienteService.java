package com.mol.routing_gui.backend.service;

import com.mol.routing_gui.backend.entities.Ambiente;
import com.mol.routing_gui.backend.repository.AmbienteRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AmbienteService {
    private static final Logger LOGGER = Logger.getLogger(AmbienteService.class.getName());
    private final AmbienteRepository ambienteRepository;

    public AmbienteService(AmbienteRepository ambienteRepository) {
        this.ambienteRepository = ambienteRepository;
    }

    public List<Ambiente> findAll(String filterText) {
        if(filterText==null || filterText.isEmpty())
            return ambienteRepository.findAll();
        else
            return ambienteRepository.search(filterText);
    }
    public List<Ambiente> findAll() {
        return ambienteRepository.findAll();
    }

    public void save(Ambiente ambiente) {
        if (ambiente == null) {
            LOGGER.log(Level.SEVERE,
                    "ERRORE: Ambiente nullo");
            return;
        }
        if(findAll(ambiente.getRiferimento()).size()==0)
            ambienteRepository.save(ambiente);
    }
}
