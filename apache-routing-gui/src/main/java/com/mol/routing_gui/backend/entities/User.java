package com.mol.routing_gui.backend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

//UTENTI
@Entity
@Table(name = "User")
public class User extends AbstractEntity implements Cloneable{

    @NotNull
    @NotEmpty
    @Column
    String username;

    @NotNull
    @NotEmpty
    @Column
    String password;

    @NotNull
    @NotEmpty
    String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getPassword(){
        return password;
    }

}
