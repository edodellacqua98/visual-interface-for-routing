package com.mol.routing_gui.backend.utilities;

import com.mol.routing_gui.backend.entities.Ambiente;
import com.mol.routing_gui.backend.entities.Partner;
import com.mol.routing_gui.backend.service.AmbienteService;
import com.mol.routing_gui.backend.service.PartnerService;

import java.io.IOException;
import java.util.List;

//Comandi per la gestione delle routing policies
public class ShellCommands {

    //Ottiene un elenco delle policies definite su Apache
    public static void getRoutes(PartnerService partnerService, AmbienteService ambienteService) throws IOException {
        ProcessBuilder pb = new ProcessBuilder("docker", "exec", "-i", "test", "/etc/apache2/scripts/get-active-routes.sh");
        Process process = pb.start();
        String error = (new String(process.getErrorStream().readAllBytes()));
        //Se è presente un errore, viene restituito in output
        if(error.length()!=0){
            if(error.contains("No such file or directory"))
                partnerService.findAll().forEach(partner -> {partnerService.delete(partner);partner.setAmbiente(null); partnerService.save(partner);});
            else
                System.err.println(error);
            return;
        }
        String result = new String(process.getInputStream().readAllBytes());
        String[] lines = result.split(System.getProperty("line.separator"));
        for(String s: lines){
            String[] output = s.split(" - ");
            List<Partner> partns = partnerService.findAll(output[0]);
            Partner part = partns.size()==0? null: partns.get(0);
            List<Ambiente> ambienti = ambienteService.findAll(output[1]);
            Ambiente am = ambienti.size()==0?null: ambienti.get(0);
            //Se c'è una regola definita per un partner inesistente nel db, il partner viene salvato
            if(part==null) {
                part = new Partner();
                part.setCompagnia(output[0]);
                part.setIp(output[0]);
                part.setAmbiente(am);
                partnerService.save(part);
                return;
            }
            //Altrimenti cambia l'ambiente assegnato al partner nel db
            partnerService.delete(part);
            part.setAmbiente(am);
            partnerService.save(part);
        }
    }

    //Modifica una regola
    public static void refreshRoute(String ip, String ambiente) throws IOException, InterruptedException {
        ProcessBuilder pb1 = new ProcessBuilder("docker", "exec", "-i", "test", "/etc/apache2/scripts/delete-rule.sh", ip, "reload");
        ProcessBuilder pb2 = new ProcessBuilder("docker", "exec", "-i", "test", "/etc/apache2/scripts/route-partner.sh", ip, ambiente, "reload");
        Process pr1 = pb1.start();
        pr1.waitFor(); //Gestione delle race conditions
        if(ambiente==null) {
            deleteRoute(ip);
            return;
        }
        Process pr2 = pb2.start();
        pr2.waitFor();
    }

    //Elimina una regola
    public static void deleteRoute(String ip) throws IOException, InterruptedException {
        new ProcessBuilder("docker", "exec", "-i", "test", "/etc/apache2/scripts/delete-rule.sh", ip, "reload").start();
    }
}
