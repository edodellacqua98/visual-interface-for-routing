package com.mol.routing_gui.ui.view.ambiente_list;

import com.mol.routing_gui.backend.entities.Ambiente;
import com.mol.routing_gui.backend.service.AmbienteService;
import com.mol.routing_gui.ui.MainLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//Pagina con l'elenco degli ambienti
@Component
@Scope("prototype")
@Route(value = "Ambienti", layout = MainLayout.class)
@PageTitle("Ambienti | R-GUI")
public class AmbienteListView extends VerticalLayout {

        private final AmbienteService ambienteService;
        public Grid<Ambiente> grid = new Grid<>(Ambiente.class);
        private final TextField filterNome = new TextField();

        public AmbienteListView(AmbienteService ambienteService) {
            this.ambienteService = ambienteService;
            addClassName("ambiente_list-view"); //Classe CSS
            setSizeFull();
            configureGrid();
            configureFilterNome();

            HorizontalLayout filters = new HorizontalLayout();
            filters.add(filterNome);

            Div filt = new Div(filters);

            Div content = new Div(grid);
            content.addClassName("content");
            content.setSizeFull();

            add(filt, content);
            updateList();
        }

    private void configureFilterNome() {
            filterNome.setPlaceholder("Filtra per riferimento...");
            filterNome.setClearButtonVisible(true);
            filterNome.setValueChangeMode(ValueChangeMode.LAZY);
            filterNome.addValueChangeListener(e -> updateList());
    }


    private void configureGrid() {
            grid.addClassName("partner-grid");
            grid.setSizeFull();
            grid.removeAllColumns();
            grid.setColumns("nome", "descrizione", "riferimento");
    }

    private void updateList() {
            grid.setItems(ambienteService.findAll(filterNome.getValue()));
        }
    }
