package com.mol.routing_gui.backend.repository;

import com.mol.routing_gui.backend.entities.Partner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface PartnerRepository extends JpaRepository<Partner, Long> {
    //Ricerca per filtro
    @Query("select c from Partner c " +
            "where lower(c.ip) like lower(concat('%', :searchTerm, '%'))")
    List<Partner> search(@Param("searchTerm") String searchTerm);

    //Ricerca per evitare duplicati
    @Query("select c from Partner c where (c.ip) = lower(:searchTerm)")
    Partner findOne(@Param("searchTerm") String searchTerm);

    //Modifica dei partner
    @Transactional
    @Modifying
    @Query("update Partner c set c.compagnia = lower(:T1), c.ip = lower(:T2), c.logo= :Logo " +
            "where c.id = :ID")
    void update(@Param("T1") String T1, @Param("T2") String T2, @Param("ID") Long id, @Param("Logo") byte[] b) throws DataIntegrityViolationException;
}
