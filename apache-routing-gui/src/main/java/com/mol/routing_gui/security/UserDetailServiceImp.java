package com.mol.routing_gui.security;

import com.mol.routing_gui.backend.entities.User;
import com.mol.routing_gui.backend.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

//Definisce la fonte dei dati per l'autenticazione
public class UserDetailServiceImp implements UserDetailsService {
    private final UserService userService;

    public UserDetailServiceImp(UserService userService) {
        this.userService = userService;
    }

    //Carica l'utente dal database
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userService.findAll(username).get(0);

        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        if (user != null) {
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.password(user.getPassword());
            builder.roles(user.getRoles());
        } else {
            throw new UsernameNotFoundException("Utente non trovato!");
        }

        return builder.build();
    }
}
