package com.mol.routing_gui.view.list;

import com.mol.routing_gui.backend.entities.Partner;
import com.mol.routing_gui.ui.view.partner_list.PartnerForm;
import com.mol.routing_gui.ui.view.partner_list.PartnerListView;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.provider.ListDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//Testa la pagina contenente i Partner
@RunWith(SpringRunner.class)
@SpringBootTest
public class ListViewTest {

    @Autowired
    private PartnerListView listView;

    //Controlla che il form venga mostrato quando viene selezionato un partner dalla tabella
    @Test
    public void formShownWhenContactSelected() {
        Grid<Partner> grid = listView.grid;
        Partner primoPartner = getFirstItem(grid);

        PartnerForm form = listView.form;

        Assert.assertFalse(form.isVisible());
        grid.asSingleSelect().setValue(primoPartner);
        Assert.assertTrue(form.isVisible());
        Assert.assertEquals(primoPartner.getCompagnia(), form.compagnia.getValue());
    }

    //Prende il primo partner della tabella
    private Partner getFirstItem(Grid<Partner> grid) {
        return( (ListDataProvider<Partner>) grid.getDataProvider()).getItems().iterator().next();
    }
}

