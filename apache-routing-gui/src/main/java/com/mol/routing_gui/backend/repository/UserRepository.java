package com.mol.routing_gui.backend.repository;

import com.mol.routing_gui.backend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    //Ricerca per filro
    @Query("select u from User u " +
            "where lower(u.username) like lower(concat('%', :searchTerm, '%'))")
    List<User> search(@Param("searchTerm") String searchTerm); //
}