package com.mol.routing_gui.backend.service;

import com.mol.routing_gui.backend.entities.User;
import com.mol.routing_gui.backend.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserService {

    private static final Logger LOGGER = Logger.getLogger(PartnerService.class.getName());
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll(String filterText) {
        if(filterText==null || filterText.isEmpty())
            return userRepository.findAll();
        else
            return userRepository.search(filterText);
    }

    public void save(User user) {
        if (user == null) {
            LOGGER.log(Level.SEVERE,
                    "ERRORE: Utente nullo");
            return;
        }
        if(findAll(user.getUsername()).size()==0){
            //La password viene salvata codificata
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            userRepository.save(user);
        }
    }

    @PostConstruct
    //Inizializzazione dei dati di test
    public void getData() {
        User u = new User();
        u.setUsername("user");
        u.setPassword("password");
        u.setRoles("admin");
        save(u);

        User us = new User();
        us.setUsername("admin");
        us.setPassword("psw");
        us.setRoles("admin");
        save(us);

        User ud = new User();
        ud.setUsername("edoardo");
        ud.setPassword("tirocinio");
        ud.setRoles("admin");
        save(ud);
    }
}
